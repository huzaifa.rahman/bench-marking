#include <iostream>
#include <cmath>
#include <numeric>
#include <vector>
#include <list>
#include <benchmark/benchmark.h>

using namespace std;

uint64_t sum_of_vector_squared(int num_entries)
{
  vector<int> my_vector, my_vector_sqared;
  srand((unsigned)time(0));

  for (int i = 0; i <= num_entries; ++i)
    my_vector.push_back(rand());

  for (int i = 0; i <= num_entries; ++i)
    my_vector_sqared.push_back(pow(my_vector.at(i), 2));

  return accumulate(my_vector_sqared.begin(), my_vector_sqared.end(), 0);
}

uint64_t sum_of_list_squared(int num_entries)
{
  list<int> my_list, my_list_sqared;
  srand((unsigned)time(0));

  for (int i = 0; i <= num_entries; ++i)
    my_list.push_back(rand());

  for (int i = 0; i <= num_entries; ++i)
  {
    auto list_elem = my_list.begin();
    advance(list_elem, i);
    my_list_sqared.push_back(pow(*list_elem, 2));
  }

  return accumulate(my_list_sqared.begin(), my_list_sqared.end(), 0);
}

static void BM_VectorSum(benchmark::State &state)
{
  for (auto _ : state)
    sum_of_vector_squared(state.range(0));
}
BENCHMARK(BM_VectorSum)->DenseRange(1, 10000, 1000)->ReportAggregatesOnly(true)->DisplayAggregatesOnly(true)->MeasureProcessCPUTime()->UseRealTime();

static void BM_ListSum(benchmark::State &state)
{
  for (auto _ : state)
    sum_of_list_squared(state.range(0));
}
BENCHMARK(BM_ListSum)->DenseRange(1, 10000, 1000)->ReportAggregatesOnly(true)->DisplayAggregatesOnly(true)->MeasureProcessCPUTime()->UseRealTime();
