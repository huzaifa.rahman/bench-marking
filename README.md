# Bench Marking

This repo is for practicing benchmarking.

### How to configure, build and run

**Configure** :
- ``git clone`` the repo with the above mentioned link
- Run ``mkdir build`` to create a build dir for configuration and build files. 
- Navigate to ``<project_root_dir>/build``.
- Run ``cmake ..`` to congigure in ``Release`` mode.

**Build** : 
- Navigate to ``<project_root_dir>/build``.
- Run ``make -j`` to build the executable or run ``cmake --build .``.

**Execute / Run** : 
- Navigate to ``<project_root_dir>/build``.
- Run a ny of the following:
    - ``./bench_marking`` to display results on screen
    - ``./bench_marking --benchmark_out=<filename> --benchmark_out_format=<format>`` to write results to the ``filename`` file in ``format``.
    - ``../benchmark/tools/compare.py filters ./bench_marking BM_VectorSum BM_ListSum`` to compare the results of two functions.

### Linking benchmark library 

For this task.<br/>
- Clone ``benchmark`` repo in root dir using ``git submodule add <repo_link>``.
- Clone ``gtest`` repo in benchmark repo which is a dependency of benchmark lib.
- Use ``add_subdirectory()`` in root ``CMakeLists.txt`` to add benchmark lib.<br/>
- Link ``benchmark_main`` lib with target executable in cmake.
- Specify include path for the lib

### Vector and List functions

- Both functions first create containers of ``num_entries`` size then fill in random data in it.
- Then create another container to contain squared values of previous container.
- Finally the sum of the squared container is returned.

### Performance

- Vector container perfermonce is superior than list container overall.
- ``-O3`` optimization level and ``Release`` build type improves performance.
- Minor code changes also reflect in performance.


